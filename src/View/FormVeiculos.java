package View;

import Model.ListaVeiculos;
import Model.Veiculo;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class FormVeiculos extends javax.swing.JDialog {
    
    ArrayList<Veiculo> automoveis = new ArrayList<>();
    ListaVeiculos listaVeiculos = new ListaVeiculos();
    
    public FormVeiculos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        painelCadastro = new javax.swing.JPanel();
        btn_NovoVeiculo = new javax.swing.JButton();
        btn_MargemLucro = new javax.swing.JButton();
        btn_IniciarViagem = new javax.swing.JButton();
        btn_sobre = new javax.swing.JButton();
        abas = new javax.swing.JTabbedPane();
        abaVeiculosDisponiveis = new javax.swing.JPanel();
        tblVeiculosDisponiveis = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Transportadora");

        painelCadastro.setBorder(javax.swing.BorderFactory.createTitledBorder("Opções de cadastro"));
        painelCadastro.setLayout(new java.awt.GridLayout());

        btn_NovoVeiculo.setText("Novo");
        btn_NovoVeiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_NovoVeiculoActionPerformed(evt);
            }
        });
        painelCadastro.add(btn_NovoVeiculo);

        btn_MargemLucro.setText("Lucro");
        btn_MargemLucro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_MargemLucroActionPerformed(evt);
            }
        });
        painelCadastro.add(btn_MargemLucro);

        btn_IniciarViagem.setText("Nova Viagem");
        btn_IniciarViagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_IniciarViagemActionPerformed(evt);
            }
        });
        painelCadastro.add(btn_IniciarViagem);

        btn_sobre.setText("Sobre");
        btn_sobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sobreActionPerformed(evt);
            }
        });
        painelCadastro.add(btn_sobre);

        getContentPane().add(painelCadastro, java.awt.BorderLayout.CENTER);

        abaVeiculosDisponiveis.setLayout(new java.awt.BorderLayout());

        jTable1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Veiculo", "Carga", "Velocidade Media"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Double.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        tblVeiculosDisponiveis.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
        }

        abaVeiculosDisponiveis.add(tblVeiculosDisponiveis, java.awt.BorderLayout.CENTER);

        abas.addTab("Veiculos Disponíves", abaVeiculosDisponiveis);

        getContentPane().add(abas, java.awt.BorderLayout.PAGE_START);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_IniciarViagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_IniciarViagemActionPerformed
        Formfrete form = new Formfrete(null, true);
        form.setTitle("Calculo de frete");
        form.setLocationRelativeTo(null);
        form.setResizable(false);
        form.setVisible(true);
    }//GEN-LAST:event_btn_IniciarViagemActionPerformed

    private void btn_NovoVeiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_NovoVeiculoActionPerformed
        FormCadastro form = new FormCadastro(null, true);
        form.setTitle("Cadastro de Veículos");
        form.setLocationRelativeTo(null);
        form.setResizable(false);
        form.setVisible(true);
    }//GEN-LAST:event_btn_NovoVeiculoActionPerformed

    private void btn_MargemLucroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_MargemLucroActionPerformed
        FormMargemLucro form = new FormMargemLucro(null, true);
        form.setLocationRelativeTo(null);
        form.setResizable(false);
        form.setVisible(true);
    }//GEN-LAST:event_btn_MargemLucroActionPerformed

    private void btn_sobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sobreActionPerformed
        JOptionPane.showMessageDialog(null, "Projeto 2 - programação orientada a objetos\nAutor: Victor Amaral Cerqueira\nMatrícula: 17/0164411");
    }//GEN-LAST:event_btn_sobreActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormVeiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormVeiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormVeiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormVeiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FormVeiculos dialog = new FormVeiculos(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    //preenche a tabela dos veiculos
    private void carregarLista(){
                
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel abaVeiculosDisponiveis;
    private javax.swing.JTabbedPane abas;
    private javax.swing.JButton btn_IniciarViagem;
    private javax.swing.JButton btn_MargemLucro;
    private javax.swing.JButton btn_NovoVeiculo;
    private javax.swing.JButton btn_sobre;
    private javax.swing.JTable jTable1;
    private javax.swing.JPanel painelCadastro;
    private javax.swing.JScrollPane tblVeiculosDisponiveis;
    // End of variables declaration//GEN-END:variables
}
