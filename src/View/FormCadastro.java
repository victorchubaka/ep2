package View;

import Model.*;
import javax.swing.JOptionPane;

public class FormCadastro extends javax.swing.JDialog {

    
    
    public FormCadastro(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_CadastroV = new javax.swing.JLabel();
        box_Veiculos = new javax.swing.JComboBox<>();
        btn_salvar = new javax.swing.JButton();
        lbl_veiculosCadastrados = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lbl_CadastroV.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        lbl_CadastroV.setText("Cadastro de Veículos:");

        box_Veiculos.setMaximumRowCount(4);
        box_Veiculos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Moto", "Carro", "Van", "Carreta" }));

        btn_salvar.setText("Salvar");
        btn_salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salvarActionPerformed(evt);
            }
        });

        lbl_veiculosCadastrados.setText("processando...");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lbl_CadastroV))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(box_Veiculos, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_salvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lbl_veiculosCadastrados)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_CadastroV)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(box_Veiculos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_salvar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_veiculosCadastrados)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salvarActionPerformed
                ListaVeiculos veiculos = new ListaVeiculos();
                
                if (box_Veiculos.getSelectedItem().equals("Carreta")) {
                    String tipo = "Carreta";
                    double velocidade = 60.0f;
                    double carga = 30000.0f;
                    
                    Veiculo veiculo = new Veiculo(tipo, velocidade, carga);
        
                    veiculos.addVeiculo(veiculo);
        
                    lbl_veiculosCadastrados.setText(veiculos.numeroDeVeiculos()+ " Veiculo(s) Cadastrado(s)");
        
                    System.out.println(veiculo);
                    
                    JOptionPane.showMessageDialog(null, "Carreta salva com sucesso!");
                    
                } else if (box_Veiculos.getSelectedItem().equals("Van")) {
                    String tipo = "Van";
                    double velocidade = 60.0f;
                    double carga = 30000.0f;
                    
                    Veiculo veiculo = new Veiculo(tipo, velocidade, carga);
        
                    veiculos.addVeiculo(veiculo);
        
                    lbl_veiculosCadastrados.setText(veiculos.numeroDeVeiculos()+ " Veiculo(s) Cadastrado(s)");
        
                    System.out.println(veiculo);
                    JOptionPane.showMessageDialog(null, "Van salva com sucesso!");
                   
                } else if (box_Veiculos.getSelectedItem().equals("Carro")) {
                    String tipo = "Carro";
                    double velocidade = 60.0f;
                    double carga = 30000.0f;
                    
                    Veiculo veiculo = new Veiculo(tipo, velocidade, carga);
        
                    veiculos.addVeiculo(veiculo);
        
                    lbl_veiculosCadastrados.setText(veiculos.numeroDeVeiculos()+ " Veiculo(s) Cadastrado(s)");
        
                    System.out.println(veiculo);
                    JOptionPane.showMessageDialog(null, "Carro salvo com sucesso!");
                    
                } else if (box_Veiculos.getSelectedItem().equals("Moto")) {
                    String tipo = "Moto";
                    double velocidade = 60.0f;
                    double carga = 30000.0f;
                    
                    Veiculo veiculo = new Veiculo(tipo, velocidade, carga);
        
                    veiculos.addVeiculo(veiculo);
        
                    lbl_veiculosCadastrados.setText(veiculos.numeroDeVeiculos()+ " Veiculo(s) Cadastrado(s)");
        
                    System.out.println(veiculo);
                    JOptionPane.showMessageDialog(null, "Moto salva com sucesso!");
                    
                } else {
                    JOptionPane.showMessageDialog(null, "Tipo de veículo inválido, confira capitalização e tente novamente.");
                }
    }//GEN-LAST:event_btn_salvarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FormCadastro dialog = new FormCadastro(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> box_Veiculos;
    private javax.swing.JButton btn_salvar;
    private javax.swing.JLabel lbl_CadastroV;
    private javax.swing.JLabel lbl_veiculosCadastrados;
    // End of variables declaration//GEN-END:variables
}
