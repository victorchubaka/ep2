package Model;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Veiculo {
    protected String tipo;
    protected String combustivel;
    protected double rendimento;
    protected double cargaMax;
    protected float velocidadeMedia;
    protected double cargaAtual;
    protected double distancia;
    protected double tempoEntrega;
    protected double perdaRendimento;
    protected char disponibilidade;
    protected double margemLucro;
    protected int tempoLimite;
    protected double frete;
    protected double custoBeneficio;

    public Veiculo(String tipo, double cargaMax, double velocidadeMedia) {
        this.tipo = tipo;
    }
    
    public double getFrete() {
        return frete;
    }

    public void setFrete(double frete) {
        this.frete = frete;
    }

    public double getCustoBeneficio() {
        return custoBeneficio;
    }

    public void setCustoBeneficio(double custoBeneficio) {
        this.custoBeneficio = custoBeneficio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public char getDisponibilidade() {
        return disponibilidade;
    }

    public void setDisponibilidade(char disponibilidade) {
        this.disponibilidade = disponibilidade;
    }

    public int getTempoLimite() {
        return tempoLimite;
    }

    public void setTempoLimite(int tempoLimite) {
        this.tempoLimite = tempoLimite;
    }
    
    
    
    public double getTempoEntrega() {
        return tempoEntrega;
    }

    public void setTempoEntrega(double tempoEntrega) {
        this.tempoEntrega = tempoEntrega;
    }

    public double getPerdaRendimento() {
        return perdaRendimento;
    }

    public void setPerdaRendimento(double perdaRendimento) {
        this.perdaRendimento = perdaRendimento;
    }

    public double getMargemLucro() {
        return margemLucro;
    }

    public void setMargemLucro(double margemLucro) {
        this.margemLucro = margemLucro;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public double getRendimento() {
        return rendimento;
    }

    public void setRendimento(double rendimento) {
        this.rendimento = rendimento;
    }

    public double getCargaMax() {
        return cargaMax;
    }

    public void setCargaMax(double cargaMax) {
        this.cargaMax = cargaMax;
    }

    public float getVelocidadeMedia() {
        return velocidadeMedia;
    }

    public void setVelocidadeMedia(float velocidadeMedia) {
        this.velocidadeMedia = velocidadeMedia;
    }

    public double getCargaAtual() {
        return cargaAtual;
    }

    public void setCargaAtual(double cargaAtual) {
        this.cargaAtual = cargaAtual;
    }
    
    public void calcularFrete(){ // frete = 2*distancia(ida e volta) / rendimento * preço combustivel + taxa da companhia
        this.verDisponibilidade();
        this.calcTempoEntrega();
        
        if(this.getDisponibilidade() == 'D'){
        this.reducaoRendimento();
        if (this.combustivel.equals("Diesel")) {
            this.setFrete(this.distancia*2 / this.rendimento * 3.869 + this.getMargemLucro());
            System.out.printf("O valor do frete é de R$%.2f\n", this.getFrete());
        } else if(this.combustivel.equals("Gasolina")) {
            this.setFrete(this.distancia*2 / this.rendimento * 4.449 + this.getMargemLucro());
            System.out.printf("O valor do frete é de R$%.2f\n", this.getFrete());
        } else {
            this.setFrete(this.distancia*2 / this.rendimento * 3.499 + this.getMargemLucro());
            System.out.printf("O valor do frete é de R$%.2f\n", this.getFrete());
        } 
       } else {
            System.out.println("Veículo não atende aos requisitos para entrega (tempo/carga)");  
        }
    }
    
    public void verDisponibilidade(){
        if (this.tempoEntrega <= this.tempoLimite && this.cargaAtual <= this.cargaMax){
            this.setDisponibilidade('D');
        } else {
            this.setDisponibilidade('I');
        }
    }
    
    public void calcTempoEntrega(){
        int dias;
        int horas;
        int minutos;
       
        this.setTempoEntrega(this.distancia / this.velocidadeMedia);
        
        dias = (int)    this.tempoEntrega* 60  / 1440;
        horas = (int)   (this.tempoEntrega * 60 - (dias*1440))/60;
        minutos = (int) this.tempoEntrega * 60 - (dias*1440) - (horas*60);
    }
    
     public void reducaoRendimento(){
        this.rendimento -= (this.cargaAtual * this.perdaRendimento);
    }
     
    public void calcCustoBeneficio(){
        this.setCustoBeneficio(this.getTempoEntrega() / this.getFrete());
        System.out.println("custo benefício: " + this.getCustoBeneficio());
    }
    
    public String salvarVeiculo(){
        
        try {
            FileWriter fw = new FileWriter("Frota.txt", true);
            PrintWriter pw = new PrintWriter(fw);
            
            pw.println(this.tipo);
            pw.println(this.cargaMax);
            pw.println(this.velocidadeMedia);
            
            pw.flush(); //envia os arquivos para frota.txt
            pw.close();
                    
        } catch (IOException ex) {
            Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "Veículo salvo com sucesso!";
    }
    
    @Override
    public String toString() {
        return tipo + "\n" + cargaMax + "\n" + velocidadeMedia;
    }
}
