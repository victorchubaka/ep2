package controller;

import Model.*;
import View.*;

import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Scanner;
import java.io.*;

public class controller {

    public static void main(String args[])
    {
        new FormInicial().initComponents();
    }

    public static void doShipping(float length, float carga, float time) throws IOException {
        int carro = 0, carreta = 0, van = 0, moto = 0;
        Scanner checkTypesVehicles = new Scanner("Frota.txt");
        while(checkTypesVehicles.hasNextLine())
        {
            String type = checkTypesVehicles.useDelimiter("-").next();
            if (type.equals("Carro")) {
                carro++;
                System.out.printf("Debug carro");
            } else if (type.equals("Carreta")) {
                System.out.printf("Debug carreta");
                carreta++;
            } else if (type.equals("Van")) {
                System.out.printf("Debug van");
                van++;
            } else if (type.equals("Moto")) {
                System.out.printf("Debug moto");
                moto++;
            }
        }
        checkTypesVehicles.close();
        BufferedReader reader = new BufferedReader(new FileReader("lucro.txt"));
        StringBuilder stringBuilder = new StringBuilder();
        String l;
        String ls = System.getProperty("line.separator");
        while((l=reader.readLine())!=null)
        {
            stringBuilder.append(l);
            stringBuilder.append(ls);
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        reader.close();
        String lucro = stringBuilder.toString();
    }
    
    //Métodos para editar .Txt
    public static void clearFrota()
    {
        try
        {
            FileWriter writer = new FileWriter("Frota.txt", false);
            writer.write("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static boolean saveCarreta(String nome)
    {
        try
        {
            FileWriter writer = new FileWriter("Frota.txt", true);
            writer.write(nome);
            writer.write("\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
    public static boolean saveVan(String nome)
    {
        try
        {
            FileWriter writer = new FileWriter("Frota.txt", true);
            writer.write(nome);
            writer.write("\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
    public static boolean saveCarro(String nome)
    {
        try
        {
            FileWriter writer = new FileWriter("Frota.txt", true);
            writer.write(nome);
            writer.write("\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
    public static boolean saveMoto(String nome)
    {
        try
        {
            FileWriter writer = new FileWriter("Frota.txt", true);
            writer.write(nome);
            writer.write("\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
    public static void mudarMargemLucro(float margem)
    {
        try
        {
            FileWriter writer = new FileWriter("lucro.txt", false);
            String margem1 = Float.toString(margem);
            writer.write(margem1);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 }
