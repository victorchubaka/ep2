package programatransportadora;

public abstract class Veiculo {
    protected String combustivel;
    protected double rendimento;
    protected double cargaMax;
    protected int velocidadeMedia;
    protected double cargaAtual;
    protected double distancia;
    protected double tempoEntrega;
    protected double perdaRendimento;
    protected boolean disponibilidade;
    protected double margemLucro;
    protected int tempoLimite;
    protected double frete;
    protected double custoBeneficio;
    
    public void calcularFrete(){ // frete = 2*distancia(ida e volta) / rendimento * preço combustivel + taxa da companhia
        this.verDisponibilidade();
        this.calcTempoEntrega();
        
        if(this.getDisponibilidade()){
        this.reducaoRendimento();
        if (this.combustivel.equals("Diesel")) {
            this.setFrete(this.distancia*2 / this.rendimento * 3.869 + this.getMargemLucro());
            System.out.printf("O valor do frete é de R$%.2f\n", this.getFrete());
        } else if(this.combustivel.equals("Gasolina")) {
            this.setFrete(this.distancia*2 / this.rendimento * 4.449 + this.getMargemLucro());
            System.out.printf("O valor do frete é de R$%.2f\n", this.getFrete());
        } else {
            this.setFrete(this.distancia*2 / this.rendimento * 3.499 + this.getMargemLucro());
            System.out.printf("O valor do frete é de R$%.2f\n", this.getFrete());
        } 
       } else {
            System.out.println("Veículo não atende aos requisitos para entrega (tempo/carga)");  
        }
    }
    
    public void verDisponibilidade(){
        if (this.tempoEntrega <= this.tempoLimite && this.cargaAtual <= this.cargaMax){
            this.setDisponibilidade(true);
        } else {
            this.setDisponibilidade(false);
        }
    }
    
    public void calcTempoEntrega(){
        int dias;
        int horas;
        int minutos;
       
        this.setTempoEntrega(this.distancia / this.velocidadeMedia);
        
        dias = (int)    this.tempoEntrega* 60  / 1440;
        horas = (int)   (this.tempoEntrega * 60 - (dias*1440))/60;
        minutos = (int) this.tempoEntrega * 60 - (dias*1440) - (horas*60);
    }
    
    public void reducaoRendimento(){
        this.rendimento -= (this.cargaAtual * this.perdaRendimento);
    }
    
    public void calcCustoBeneficio(){
        this.setCustoBeneficio(this.getTempoEntrega() / this.getFrete());
        System.out.println("custo benefício: " + this.getCustoBeneficio());
    }

    public double getFrete() {
        return frete;
    }

    public void setFrete(double frete) {
        this.frete = frete;
    }

    public double getCustoBeneficio() {
        return custoBeneficio;
    }

    public void setCustoBeneficio(double custoBeneficio) {
        this.custoBeneficio = custoBeneficio;
    }
    
    public boolean getDisponibilidade() {
        return disponibilidade;
    }

    public void setDisponibilidade(boolean disponibilidade) {
        this.disponibilidade = disponibilidade;
    }

    public int getTempoLimite() {
        return tempoLimite;
    }

    public void setTempoLimite(int tempoLimite) {
        this.tempoLimite = tempoLimite;
    }
    
    
    
    public double getTempoEntrega() {
        return tempoEntrega;
    }

    public void setTempoEntrega(double tempoEntrega) {
        this.tempoEntrega = tempoEntrega;
    }

    public double getPerdaRendimento() {
        return perdaRendimento;
    }

    public void setPerdaRendimento(double perdaRendimento) {
        this.perdaRendimento = perdaRendimento;
    }

    public double getMargemLucro() {
        return margemLucro;
    }

    public void setMargemLucro(double margemLucro) {
        this.margemLucro = margemLucro;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public double getRendimento() {
        return rendimento;
    }

    public void setRendimento(double rendimento) {
        this.rendimento = rendimento;
    }

    public double getCargaMax() {
        return cargaMax;
    }

    public void setCargaMax(double cargaMax) {
        this.cargaMax = cargaMax;
    }

    public int getVelocidadeMedia() {
        return velocidadeMedia;
    }

    public void setVelocidadeMedia(int velocidadeMedia) {
        this.velocidadeMedia = velocidadeMedia;
    }

    public double getCargaAtual() {
        return cargaAtual;
    }

    public void setCargaAtual(double cargaAtual) {
        this.cargaAtual = cargaAtual;
    }
}
