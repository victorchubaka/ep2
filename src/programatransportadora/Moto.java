package programatransportadora;

public class Moto extends Veiculo {
    private double frete1;
    private double frete2;
    
    public Moto() {
        this.cargaMax = 50.0f;
        this.velocidadeMedia = 110;
    }
    
    @Override
    public void calcularFrete(){ // frete = 2*distancia(ida e volta) / rendimento * preço combustivel + taxa da companhia
        this.verDisponibilidade();
        this.calcTempoEntrega();
        
        if(this.getDisponibilidade()){
            
            this.combustivel = "Gasolina";
            this.perdaRendimento = 0.3f;
            this.rendimento = 50.0f;
            this.reducaoRendimento();
            
            
            this.setFrete1(this.distancia*2 / this.rendimento * 4.449 + this.getMargemLucro());
            
            this.combustivel = "Alcool";
            this.perdaRendimento = 0.4f;
            this.rendimento = 43.0f;
            this.reducaoRendimento();
            
            this.setFrete2(this.distancia*2 / this.rendimento * 3.499 + this.getMargemLucro());
            
            if(this.getFrete1() <= this.getFrete2()){
                this.setFrete(this.getFrete1());
            System.out.printf("O valor do frete mais barato é de R$%.2f usando Gasolina\n", this.getFrete());
            } else{     
                this.setFrete(this.getFrete2());
                System.out.printf("O valor do frete mais barato é de R$%.2f usando Álcool\n", this.getFrete());
          } 
        } else {
            System.out.println("Veículo não atende aos requisitos para entrega (tempo/carga)");  
        }
    }

    public double getFrete1() {
        return frete1;
    }

    public void setFrete1(double frete1) {
        this.frete1 = frete1;
    }

    public double getFrete2() {
        return frete2;
    }

    public void setFrete2(double frete2) {
        this.frete2 = frete2;
    }
    
    
}
