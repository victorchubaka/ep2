package programatransportadora;

public class Carreta extends Veiculo {

    public Carreta() {
        this.combustivel = "Diesel";
        this.cargaMax = 30000.0f;
        this.velocidadeMedia = 60;
        this.rendimento = 8.0f;
        this.perdaRendimento = 0.0002f;
        this.disponibilidade = false;
    }    
}
