package programatransportadora;

public class ProgramaTransportadora {
    
    public static void main(String[] args) {
        Carreta c1 = new Carreta();
        Van v1 = new Van();
        Carro ca1 = new Carro();
        Moto m1 = new Moto();
        
        m1.setMargemLucro(600);
        m1.setTempoLimite(300);
        m1.setCargaAtual(10.0f);
        m1.setDistancia(100);
        
        m1.calcTempoEntrega();
        m1.calcularFrete();
        m1.calcCustoBeneficio();
    }
    
}
