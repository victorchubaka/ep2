package programatransportadora;

public class Van extends Veiculo{

    public Van() {
        this.combustivel = "Diesel";
        this.cargaMax = 3500.0f;
        this.velocidadeMedia = 80;
        this.rendimento = 10.0f;
        this.perdaRendimento = 0.001f;
    }
    
    
}
